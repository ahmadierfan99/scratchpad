#if 1
#if !defined(NDEBUG) && !defined(_DEBUG)
#error "Define at least one."
#elif defined(NDEBUG) && defined(_DEBUG)
#error "Define at most one."
#endif

#if defined(_WIN64)
#if defined(_DEBUG)
#pragma comment (lib, "lib/64/SDL2-staticd")
#else
#pragma comment (lib, "lib/64/SDL2-static")
#endif
#pragma comment (lib, "lib/64/vulkan-1")
#else
#if defined(_DEBUG)
#pragma comment (lib, "lib/32/SDL2-staticd")
#else
#pragma comment (lib, "lib/32/SDL2-static")
#endif
#pragma comment (lib, "lib/32/vulkan-1")
#endif
#pragma comment (lib, "Imm32")
#pragma comment (lib, "Setupapi")
#pragma comment (lib, "Version")
#pragma comment (lib, "Winmm")

#define _CRT_SECURE_NO_WARNINGS 1

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#define VK_USE_PLATFORM_WIN32_KHR
#pragma warning (push)
#pragma warning (disable: 26812)    // The enum type ... is unscoped. Prefer 'enum class' over 'enum' (Enum.3).
#include "include/vulkan/vulkan.h"
#pragma warning (pop)

#define SDL_MAIN_HANDLED
#include "include/SDL2/SDL.h"
//#include "include/SDL2/SDL_syswm.window_height"     // SDL_SysWMInfo, ...
#include "include/SDL2/SDL_vulkan.h"

#include <cstdio>
#include <algorithm>

//#define WIN32_LEAN_AND_MEAN
//#include <Windows.h>        // GetModuleHandle()

#if defined(_DEBUG)
#define ENABLE_VALIDATION
#endif

#define VULKAN_CHECK_AND_FAIL(func_)                \
    if (VK_SUCCESS != vk_res) {                     \
        ::printf("[ERROR] " #func_ "() failed. \n");\
        ::abort();                                  \
    }                                               \
    /**/

// Some code from:
//  https://sopyer.github.io/b/post/minimal-vulkan-sample/
//  https://github.com/sopyer/Vulkan/blob/562e653fbbd1f7a83ec050676b744dd082b2ebed/main.c
PFN_vkCreateDebugReportCallbackEXT vk_CreateDebugReportCallbackEXT = nullptr;
PFN_vkDestroyDebugReportCallbackEXT vk_DestroyDebugReportCallbackEXT = nullptr;
PFN_vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR vk_EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR = nullptr;

VkDebugReportCallbackEXT yvkDebugCallback = VK_NULL_HANDLE;

static VKAPI_ATTR VkBool32 VKAPI_CALL
VulkanReportFunc(
	VkDebugReportFlagsEXT flags,
	VkDebugReportObjectTypeEXT objType,
	uint64_t obj,
	size_t location,
	int32_t code,
	char const* layerPrefix,
	char const* msg,
	void* userData
)
{
	char flag_str[6] = "-----";
	if (flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT) flag_str[0] = 'I';
	if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT) flag_str[1] = 'W';
	if (flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) flag_str[2] = 'P';
	if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) flag_str[3] = 'E';
	if (flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT) flag_str[4] = 'D';

	printf("[[VULKAN: %s]] %s\n", flag_str, msg);
	return VK_FALSE;
}

const char* VkFormatToString(const VkFormat& format)
{
	switch (format)
	{
		case VK_FORMAT_UNDEFINED: return "VK_FORMAT_UNDEFINED";
		case VK_FORMAT_R4G4_UNORM_PACK8: return "VK_FORMAT_R4G4_UNORM_PACK8";
		case VK_FORMAT_R4G4B4A4_UNORM_PACK16: return "VK_FORMAT_R4G4B4A4_UNORM_PACK16";
		case VK_FORMAT_B4G4R4A4_UNORM_PACK16: return "VK_FORMAT_B4G4R4A4_UNORM_PACK16";
		case VK_FORMAT_R5G6B5_UNORM_PACK16: return "VK_FORMAT_R5G6B5_UNORM_PACK16";
		case VK_FORMAT_B5G6R5_UNORM_PACK16: return "VK_FORMAT_B5G6R5_UNORM_PACK16";
		case VK_FORMAT_R5G5B5A1_UNORM_PACK16: return "VK_FORMAT_R5G5B5A1_UNORM_PACK16";
		case VK_FORMAT_B5G5R5A1_UNORM_PACK16: return "VK_FORMAT_B5G5R5A1_UNORM_PACK16";
		case VK_FORMAT_A1R5G5B5_UNORM_PACK16: return "VK_FORMAT_A1R5G5B5_UNORM_PACK16";
		case VK_FORMAT_R8_UNORM: return "VK_FORMAT_R8_UNORM";
		case VK_FORMAT_R8_SNORM: return "VK_FORMAT_R8_SNORM";
		case VK_FORMAT_R8_USCALED: return "VK_FORMAT_R8_USCALED";
		case VK_FORMAT_R8_SSCALED: return "VK_FORMAT_R8_SSCALED";
		case VK_FORMAT_R8_UINT: return "VK_FORMAT_R8_UINT";
		case VK_FORMAT_R8_SINT: return "VK_FORMAT_R8_SINT";
		case VK_FORMAT_R8_SRGB: return "VK_FORMAT_R8_SRGB";
		case VK_FORMAT_R8G8_UNORM: return "VK_FORMAT_R8G8_UNORM";
		case VK_FORMAT_R8G8_SNORM: return "VK_FORMAT_R8G8_SNORM";
		case VK_FORMAT_R8G8_USCALED: return "VK_FORMAT_R8G8_USCALED";
		case VK_FORMAT_R8G8_SSCALED: return "VK_FORMAT_R8G8_SSCALED";
		case VK_FORMAT_R8G8_UINT: return "VK_FORMAT_R8G8_UINT";
		case VK_FORMAT_R8G8_SINT: return "VK_FORMAT_R8G8_SINT";
		case VK_FORMAT_R8G8_SRGB: return "VK_FORMAT_R8G8_SRGB";
		case VK_FORMAT_R8G8B8_UNORM: return "VK_FORMAT_R8G8B8_UNORM";
		case VK_FORMAT_R8G8B8_SNORM: return "VK_FORMAT_R8G8B8_SNORM";
		case VK_FORMAT_R8G8B8_USCALED: return "VK_FORMAT_R8G8B8_USCALED";
		case VK_FORMAT_R8G8B8_SSCALED: return "VK_FORMAT_R8G8B8_SSCALED";
		case VK_FORMAT_R8G8B8_UINT: return "VK_FORMAT_R8G8B8_UINT";
		case VK_FORMAT_R8G8B8_SINT: return "VK_FORMAT_R8G8B8_SINT";
		case VK_FORMAT_R8G8B8_SRGB: return "VK_FORMAT_R8G8B8_SRGB";
		case VK_FORMAT_B8G8R8_UNORM: return "VK_FORMAT_B8G8R8_UNORM";
		case VK_FORMAT_B8G8R8_SNORM: return "VK_FORMAT_B8G8R8_SNORM";
		case VK_FORMAT_B8G8R8_USCALED: return "VK_FORMAT_B8G8R8_USCALED";
		case VK_FORMAT_B8G8R8_SSCALED: return "VK_FORMAT_B8G8R8_SSCALED";
		case VK_FORMAT_B8G8R8_UINT: return "VK_FORMAT_B8G8R8_UINT";
		case VK_FORMAT_B8G8R8_SINT: return "VK_FORMAT_B8G8R8_SINT";
		case VK_FORMAT_B8G8R8_SRGB: return "VK_FORMAT_B8G8R8_SRGB";
		case VK_FORMAT_R8G8B8A8_UNORM: return "VK_FORMAT_R8G8B8A8_UNORM";
		case VK_FORMAT_R8G8B8A8_SNORM: return "VK_FORMAT_R8G8B8A8_SNORM";
		case VK_FORMAT_R8G8B8A8_USCALED: return "VK_FORMAT_R8G8B8A8_USCALED";
		case VK_FORMAT_R8G8B8A8_SSCALED: return "VK_FORMAT_R8G8B8A8_SSCALED";
		case VK_FORMAT_R8G8B8A8_UINT: return "VK_FORMAT_R8G8B8A8_UINT";
		case VK_FORMAT_R8G8B8A8_SINT: return "VK_FORMAT_R8G8B8A8_SINT";
		case VK_FORMAT_R8G8B8A8_SRGB: return "VK_FORMAT_R8G8B8A8_SRGB";
		case VK_FORMAT_B8G8R8A8_UNORM: return "VK_FORMAT_B8G8R8A8_UNORM";
		case VK_FORMAT_B8G8R8A8_SNORM: return "VK_FORMAT_B8G8R8A8_SNORM";
		case VK_FORMAT_B8G8R8A8_USCALED: return "VK_FORMAT_B8G8R8A8_USCALED";
		case VK_FORMAT_B8G8R8A8_SSCALED: return "VK_FORMAT_B8G8R8A8_SSCALED";
		case VK_FORMAT_B8G8R8A8_UINT: return "VK_FORMAT_B8G8R8A8_UINT";
		case VK_FORMAT_B8G8R8A8_SINT: return "VK_FORMAT_B8G8R8A8_SINT";
		case VK_FORMAT_B8G8R8A8_SRGB: return "VK_FORMAT_B8G8R8A8_SRGB";
		case VK_FORMAT_A8B8G8R8_UNORM_PACK32: return "VK_FORMAT_A8B8G8R8_UNORM_PACK32";
		case VK_FORMAT_A8B8G8R8_SNORM_PACK32: return "VK_FORMAT_A8B8G8R8_SNORM_PACK32";
		case VK_FORMAT_A8B8G8R8_USCALED_PACK32: return "VK_FORMAT_A8B8G8R8_USCALED_PACK32";
		case VK_FORMAT_A8B8G8R8_SSCALED_PACK32: return "VK_FORMAT_A8B8G8R8_SSCALED_PACK32";
		case VK_FORMAT_A8B8G8R8_UINT_PACK32: return "VK_FORMAT_A8B8G8R8_UINT_PACK32";
		case VK_FORMAT_A8B8G8R8_SINT_PACK32: return "VK_FORMAT_A8B8G8R8_SINT_PACK32";
		case VK_FORMAT_A8B8G8R8_SRGB_PACK32: return "VK_FORMAT_A8B8G8R8_SRGB_PACK32";
		case VK_FORMAT_A2R10G10B10_UNORM_PACK32: return "VK_FORMAT_A2R10G10B10_UNORM_PACK32";
		case VK_FORMAT_A2R10G10B10_SNORM_PACK32: return "VK_FORMAT_A2R10G10B10_SNORM_PACK32";
		case VK_FORMAT_A2R10G10B10_USCALED_PACK32: return "VK_FORMAT_A2R10G10B10_USCALED_PACK32";
		case VK_FORMAT_A2R10G10B10_SSCALED_PACK32: return "VK_FORMAT_A2R10G10B10_SSCALED_PACK32";
		case VK_FORMAT_A2R10G10B10_UINT_PACK32: return "VK_FORMAT_A2R10G10B10_UINT_PACK32";
		case VK_FORMAT_A2R10G10B10_SINT_PACK32: return "VK_FORMAT_A2R10G10B10_SINT_PACK32";
		case VK_FORMAT_A2B10G10R10_UNORM_PACK32: return "VK_FORMAT_A2B10G10R10_UNORM_PACK32";
		case VK_FORMAT_A2B10G10R10_SNORM_PACK32: return "VK_FORMAT_A2B10G10R10_SNORM_PACK32";
		case VK_FORMAT_A2B10G10R10_USCALED_PACK32: return "VK_FORMAT_A2B10G10R10_USCALED_PACK32";
		case VK_FORMAT_A2B10G10R10_SSCALED_PACK32: return "VK_FORMAT_A2B10G10R10_SSCALED_PACK32";
		case VK_FORMAT_A2B10G10R10_UINT_PACK32: return "VK_FORMAT_A2B10G10R10_UINT_PACK32";
		case VK_FORMAT_A2B10G10R10_SINT_PACK32: return "VK_FORMAT_A2B10G10R10_SINT_PACK32";
		case VK_FORMAT_R16_UNORM: return "VK_FORMAT_R16_UNORM";
		case VK_FORMAT_R16_SNORM: return "VK_FORMAT_R16_SNORM";
		case VK_FORMAT_R16_USCALED: return "VK_FORMAT_R16_USCALED";
		case VK_FORMAT_R16_SSCALED: return "VK_FORMAT_R16_SSCALED";
		case VK_FORMAT_R16_UINT: return "VK_FORMAT_R16_UINT";
		case VK_FORMAT_R16_SINT: return "VK_FORMAT_R16_SINT";
		case VK_FORMAT_R16_SFLOAT: return "VK_FORMAT_R16_SFLOAT";
		case VK_FORMAT_R16G16_UNORM: return "VK_FORMAT_R16G16_UNORM";
		case VK_FORMAT_R16G16_SNORM: return "VK_FORMAT_R16G16_SNORM";
		case VK_FORMAT_R16G16_USCALED: return "VK_FORMAT_R16G16_USCALED";
		case VK_FORMAT_R16G16_SSCALED: return "VK_FORMAT_R16G16_SSCALED";
		case VK_FORMAT_R16G16_UINT: return "VK_FORMAT_R16G16_UINT";
		case VK_FORMAT_R16G16_SINT: return "VK_FORMAT_R16G16_SINT";
		case VK_FORMAT_R16G16_SFLOAT: return "VK_FORMAT_R16G16_SFLOAT";
		case VK_FORMAT_R16G16B16_UNORM: return "VK_FORMAT_R16G16B16_UNORM";
		case VK_FORMAT_R16G16B16_SNORM: return "VK_FORMAT_R16G16B16_SNORM";
		case VK_FORMAT_R16G16B16_USCALED: return "VK_FORMAT_R16G16B16_USCALED";
		case VK_FORMAT_R16G16B16_SSCALED: return "VK_FORMAT_R16G16B16_SSCALED";
		case VK_FORMAT_R16G16B16_UINT: return "VK_FORMAT_R16G16B16_UINT";
		case VK_FORMAT_R16G16B16_SINT: return "VK_FORMAT_R16G16B16_SINT";
		case VK_FORMAT_R16G16B16_SFLOAT: return "VK_FORMAT_R16G16B16_SFLOAT";
		case VK_FORMAT_R16G16B16A16_UNORM: return "VK_FORMAT_R16G16B16A16_UNORM";
		case VK_FORMAT_R16G16B16A16_SNORM: return "VK_FORMAT_R16G16B16A16_SNORM";
		case VK_FORMAT_R16G16B16A16_USCALED: return "VK_FORMAT_R16G16B16A16_USCALED";
		case VK_FORMAT_R16G16B16A16_SSCALED: return "VK_FORMAT_R16G16B16A16_SSCALED";
		case VK_FORMAT_R16G16B16A16_UINT: return "VK_FORMAT_R16G16B16A16_UINT";
		case VK_FORMAT_R16G16B16A16_SINT: return "VK_FORMAT_R16G16B16A16_SINT";
		case VK_FORMAT_R16G16B16A16_SFLOAT: return "VK_FORMAT_R16G16B16A16_SFLOAT";
		case VK_FORMAT_R32_UINT: return "VK_FORMAT_R32_UINT";
		case VK_FORMAT_R32_SINT: return "VK_FORMAT_R32_SINT";
		case VK_FORMAT_R32_SFLOAT: return "VK_FORMAT_R32_SFLOAT";
		case VK_FORMAT_R32G32_UINT: return "VK_FORMAT_R32G32_UINT";
		case VK_FORMAT_R32G32_SINT: return "VK_FORMAT_R32G32_SINT";
		case VK_FORMAT_R32G32_SFLOAT: return "VK_FORMAT_R32G32_SFLOAT";
		case VK_FORMAT_R32G32B32_UINT: return "VK_FORMAT_R32G32B32_UINT";
		case VK_FORMAT_R32G32B32_SINT: return "VK_FORMAT_R32G32B32_SINT";
		case VK_FORMAT_R32G32B32_SFLOAT: return "VK_FORMAT_R32G32B32_SFLOAT";
		case VK_FORMAT_R32G32B32A32_UINT: return "VK_FORMAT_R32G32B32A32_UINT";
		case VK_FORMAT_R32G32B32A32_SINT: return "VK_FORMAT_R32G32B32A32_SINT";
		case VK_FORMAT_R32G32B32A32_SFLOAT: return "VK_FORMAT_R32G32B32A32_SFLOAT";
		case VK_FORMAT_R64_UINT: return "VK_FORMAT_R64_UINT";
		case VK_FORMAT_R64_SINT: return "VK_FORMAT_R64_SINT";
		case VK_FORMAT_R64_SFLOAT: return "VK_FORMAT_R64_SFLOAT";
		case VK_FORMAT_R64G64_UINT: return "VK_FORMAT_R64G64_UINT";
		case VK_FORMAT_R64G64_SINT: return "VK_FORMAT_R64G64_SINT";
		case VK_FORMAT_R64G64_SFLOAT: return "VK_FORMAT_R64G64_SFLOAT";
		case VK_FORMAT_R64G64B64_UINT: return "VK_FORMAT_R64G64B64_UINT";
		case VK_FORMAT_R64G64B64_SINT: return "VK_FORMAT_R64G64B64_SINT";
		case VK_FORMAT_R64G64B64_SFLOAT: return "VK_FORMAT_R64G64B64_SFLOAT";
		case VK_FORMAT_R64G64B64A64_UINT: return "VK_FORMAT_R64G64B64A64_UINT";
		case VK_FORMAT_R64G64B64A64_SINT: return "VK_FORMAT_R64G64B64A64_SINT";
		case VK_FORMAT_R64G64B64A64_SFLOAT: return "VK_FORMAT_R64G64B64A64_SFLOAT";
		case VK_FORMAT_B10G11R11_UFLOAT_PACK32: return "VK_FORMAT_B10G11R11_UFLOAT_PACK32";
		case VK_FORMAT_E5B9G9R9_UFLOAT_PACK32: return "VK_FORMAT_E5B9G9R9_UFLOAT_PACK32";
		case VK_FORMAT_D16_UNORM: return "VK_FORMAT_D16_UNORM";
		case VK_FORMAT_X8_D24_UNORM_PACK32: return "VK_FORMAT_X8_D24_UNORM_PACK32";
		case VK_FORMAT_D32_SFLOAT: return "VK_FORMAT_D32_SFLOAT";
		case VK_FORMAT_S8_UINT: return "VK_FORMAT_S8_UINT";
		case VK_FORMAT_D16_UNORM_S8_UINT: return "VK_FORMAT_D16_UNORM_S8_UINT";
		case VK_FORMAT_D24_UNORM_S8_UINT: return "VK_FORMAT_D24_UNORM_S8_UINT";
		case VK_FORMAT_D32_SFLOAT_S8_UINT: return "VK_FORMAT_D32_SFLOAT_S8_UINT";
		case VK_FORMAT_BC1_RGB_UNORM_BLOCK: return "VK_FORMAT_BC1_RGB_UNORM_BLOCK";
		case VK_FORMAT_BC1_RGB_SRGB_BLOCK: return "VK_FORMAT_BC1_RGB_SRGB_BLOCK";
		case VK_FORMAT_BC1_RGBA_UNORM_BLOCK: return "VK_FORMAT_BC1_RGBA_UNORM_BLOCK";
		case VK_FORMAT_BC1_RGBA_SRGB_BLOCK: return "VK_FORMAT_BC1_RGBA_SRGB_BLOCK";
		case VK_FORMAT_BC2_UNORM_BLOCK: return "VK_FORMAT_BC2_UNORM_BLOCK";
		case VK_FORMAT_BC2_SRGB_BLOCK: return "VK_FORMAT_BC2_SRGB_BLOCK";
		case VK_FORMAT_BC3_UNORM_BLOCK: return "VK_FORMAT_BC3_UNORM_BLOCK";
		case VK_FORMAT_BC3_SRGB_BLOCK: return "VK_FORMAT_BC3_SRGB_BLOCK";
		case VK_FORMAT_BC4_UNORM_BLOCK: return "VK_FORMAT_BC4_UNORM_BLOCK";
		case VK_FORMAT_BC4_SNORM_BLOCK: return "VK_FORMAT_BC4_SNORM_BLOCK";
		case VK_FORMAT_BC5_UNORM_BLOCK: return "VK_FORMAT_BC5_UNORM_BLOCK";
		case VK_FORMAT_BC5_SNORM_BLOCK: return "VK_FORMAT_BC5_SNORM_BLOCK";
		case VK_FORMAT_BC6H_UFLOAT_BLOCK: return "VK_FORMAT_BC6H_UFLOAT_BLOCK";
		case VK_FORMAT_BC6H_SFLOAT_BLOCK: return "VK_FORMAT_BC6H_SFLOAT_BLOCK";
		case VK_FORMAT_BC7_UNORM_BLOCK: return "VK_FORMAT_BC7_UNORM_BLOCK";
		case VK_FORMAT_BC7_SRGB_BLOCK: return "VK_FORMAT_BC7_SRGB_BLOCK";
		case VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK: return "VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK";
		case VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK: return "VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK";
		case VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK";
		case VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK";
		case VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK";
		case VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK";
		case VK_FORMAT_EAC_R11_UNORM_BLOCK: return "VK_FORMAT_EAC_R11_UNORM_BLOCK";
		case VK_FORMAT_EAC_R11_SNORM_BLOCK: return "VK_FORMAT_EAC_R11_SNORM_BLOCK";
		case VK_FORMAT_EAC_R11G11_UNORM_BLOCK: return "VK_FORMAT_EAC_R11G11_UNORM_BLOCK";
		case VK_FORMAT_EAC_R11G11_SNORM_BLOCK: return "VK_FORMAT_EAC_R11G11_SNORM_BLOCK";
		case VK_FORMAT_ASTC_4x4_UNORM_BLOCK: return "VK_FORMAT_ASTC_4x4_UNORM_BLOCK";
		case VK_FORMAT_ASTC_4x4_SRGB_BLOCK: return "VK_FORMAT_ASTC_4x4_SRGB_BLOCK";
		case VK_FORMAT_ASTC_5x4_UNORM_BLOCK: return "VK_FORMAT_ASTC_5x4_UNORM_BLOCK";
		case VK_FORMAT_ASTC_5x4_SRGB_BLOCK: return "VK_FORMAT_ASTC_5x4_SRGB_BLOCK";
		case VK_FORMAT_ASTC_5x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_5x5_UNORM_BLOCK";
		case VK_FORMAT_ASTC_5x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_5x5_SRGB_BLOCK";
		case VK_FORMAT_ASTC_6x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_6x5_UNORM_BLOCK";
		case VK_FORMAT_ASTC_6x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_6x5_SRGB_BLOCK";
		case VK_FORMAT_ASTC_6x6_UNORM_BLOCK: return "VK_FORMAT_ASTC_6x6_UNORM_BLOCK";
		case VK_FORMAT_ASTC_6x6_SRGB_BLOCK: return "VK_FORMAT_ASTC_6x6_SRGB_BLOCK";
		case VK_FORMAT_ASTC_8x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_8x5_UNORM_BLOCK";
		case VK_FORMAT_ASTC_8x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_8x5_SRGB_BLOCK";
		case VK_FORMAT_ASTC_8x6_UNORM_BLOCK: return "VK_FORMAT_ASTC_8x6_UNORM_BLOCK";
		case VK_FORMAT_ASTC_8x6_SRGB_BLOCK: return "VK_FORMAT_ASTC_8x6_SRGB_BLOCK";
		case VK_FORMAT_ASTC_8x8_UNORM_BLOCK: return "VK_FORMAT_ASTC_8x8_UNORM_BLOCK";
		case VK_FORMAT_ASTC_8x8_SRGB_BLOCK: return "VK_FORMAT_ASTC_8x8_SRGB_BLOCK";
		case VK_FORMAT_ASTC_10x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x5_UNORM_BLOCK";
		case VK_FORMAT_ASTC_10x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x5_SRGB_BLOCK";
		case VK_FORMAT_ASTC_10x6_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x6_UNORM_BLOCK";
		case VK_FORMAT_ASTC_10x6_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x6_SRGB_BLOCK";
		case VK_FORMAT_ASTC_10x8_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x8_UNORM_BLOCK";
		case VK_FORMAT_ASTC_10x8_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x8_SRGB_BLOCK";
		case VK_FORMAT_ASTC_10x10_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x10_UNORM_BLOCK";
		case VK_FORMAT_ASTC_10x10_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x10_SRGB_BLOCK";
		case VK_FORMAT_ASTC_12x10_UNORM_BLOCK: return "VK_FORMAT_ASTC_12x10_UNORM_BLOCK";
		case VK_FORMAT_ASTC_12x10_SRGB_BLOCK: return "VK_FORMAT_ASTC_12x10_SRGB_BLOCK";
		case VK_FORMAT_ASTC_12x12_UNORM_BLOCK: return "VK_FORMAT_ASTC_12x12_UNORM_BLOCK";
		case VK_FORMAT_ASTC_12x12_SRGB_BLOCK: return "VK_FORMAT_ASTC_12x12_SRGB_BLOCK";
		case VK_FORMAT_G8B8G8R8_422_UNORM: return "VK_FORMAT_G8B8G8R8_422_UNORM";
		case VK_FORMAT_B8G8R8G8_422_UNORM: return "VK_FORMAT_B8G8R8G8_422_UNORM";
		case VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM: return "VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM";
		case VK_FORMAT_G8_B8R8_2PLANE_420_UNORM: return "VK_FORMAT_G8_B8R8_2PLANE_420_UNORM";
		case VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM: return "VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM";
		case VK_FORMAT_G8_B8R8_2PLANE_422_UNORM: return "VK_FORMAT_G8_B8R8_2PLANE_422_UNORM";
		case VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM: return "VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM";
		case VK_FORMAT_R10X6_UNORM_PACK16: return "VK_FORMAT_R10X6_UNORM_PACK16";
		case VK_FORMAT_R10X6G10X6_UNORM_2PACK16: return "VK_FORMAT_R10X6G10X6_UNORM_2PACK16";
		case VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16: return "VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16";
		case VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16: return "VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16";
		case VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16: return "VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16";
		case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16";
		case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16";
		case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16";
		case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16";
		case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16";
		case VK_FORMAT_R12X4_UNORM_PACK16: return "VK_FORMAT_R12X4_UNORM_PACK16";
		case VK_FORMAT_R12X4G12X4_UNORM_2PACK16: return "VK_FORMAT_R12X4G12X4_UNORM_2PACK16";
		case VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16: return "VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16";
		case VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16: return "VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16";
		case VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16: return "VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16";
		case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16";
		case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16";
		case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16";
		case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16";
		case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16";
		case VK_FORMAT_G16B16G16R16_422_UNORM: return "VK_FORMAT_G16B16G16R16_422_UNORM";
		case VK_FORMAT_B16G16R16G16_422_UNORM: return "VK_FORMAT_B16G16R16G16_422_UNORM";
		case VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM: return "VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM";
		case VK_FORMAT_G16_B16R16_2PLANE_420_UNORM: return "VK_FORMAT_G16_B16R16_2PLANE_420_UNORM";
		case VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM: return "VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM";
		case VK_FORMAT_G16_B16R16_2PLANE_422_UNORM: return "VK_FORMAT_G16_B16R16_2PLANE_422_UNORM";
		case VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM: return "VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM";
		case VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG";
		case VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG";
		case VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG";
		case VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG";
		case VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG";
		case VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG";
		case VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG";
		case VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG";
		case VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT";
		case VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT";
		default:
		return "???";
	}
}


constexpr char const* gc_ShaderSourceVertexWeird = R"---(
        #version 450
        #extension GL_ARB_separate_shader_objects : enable

        layout(location = 0) in vec3    inPos;
        layout(location = 1) in vec4    inColor;

        layout(push_constant) uniform pushConstants {
            float time;
        } u_pushConstants;

        layout(location = 0) out vec4   fragColor;

        void main()
        {
            vec4 pos = vec4(0);
            pos = vec4(
                sin(u_pushConstants.time * gl_InstanceIndex / 100.0) * inPos.x * 0.05f,
                abs(cos(u_pushConstants.time)) * inPos.y * 0.05f,
                inPos.z * 0.05f,
                1.0);

            gl_Position = pos + vec4(-0.9 + (gl_InstanceIndex % 19) * 0.1f, -0.9f + (gl_InstanceIndex / 19) * 0.1f, 0, 0);
            fragColor = vec4(sin(u_pushConstants.time * gl_InstanceIndex / 100.0) * inColor.x, sin(u_pushConstants.time)* inColor.y, inColor.z, inColor.w);
        }
    )---";

constexpr char const* gc_ShaderSourceVertexWeird2 = R"---(
        #version 450
        #extension GL_ARB_separate_shader_objects : enable

        layout(location = 0) in vec3    inPos;
        layout(location = 1) in vec4    inColor;

        layout(push_constant) uniform pushConstants {
            float time;
        } u_pushConstants;

        layout(location = 0) out vec4   fragColor;

        #define each_row 500

        void main()
        {
            float quad_size = 2.0f / each_row;

            vec4 pos = vec4(0);
            pos = vec4(
                sin(u_pushConstants.time) * inPos.x * quad_size,
                cos(u_pushConstants.time) * inPos.y * quad_size,
                inPos.z * quad_size,
                1.0);

            gl_Position = pos + 
                vec4(-1.0f + (gl_InstanceIndex % each_row) * (quad_size),
                -1.0f + (gl_InstanceIndex / each_row) * quad_size * 2,
                0,
                0);

            fragColor = vec4(
                abs(sin(u_pushConstants.time * (gl_InstanceIndex % 20)) / 6.0f),
                abs(cos(u_pushConstants.time) + 0.1f),
                abs(sin(u_pushConstants.time) + 0.2),
                inColor.w);
        }
    )---";

int main()
{
	/* Init SDL... */
	SDL_Init(SDL_INIT_VIDEO);
	/* Done. */

	/* Init Vulkan instance... */
	VkResult vk_res = VK_SUCCESS;

	uint32_t vk_api_version = 0;
	vkEnumerateInstanceVersion(&vk_api_version);
	::printf("[INFO] Vulkan API implementation version: %u.%u.%u\n"
		, VK_VERSION_MAJOR(vk_api_version)
		, VK_VERSION_MINOR(vk_api_version)
		, VK_VERSION_PATCH(vk_api_version)
	);
	::printf("\n");

	constexpr unsigned MaxInstanceExts = 128;
	uint32_t vk_ext_props_count = MaxInstanceExts;
	VkExtensionProperties vk_ext_props[MaxInstanceExts];
	vkEnumerateInstanceExtensionProperties(nullptr, &vk_ext_props_count, vk_ext_props);
	::printf("[INFO] Extensions provided by the Vulkan impl or implicitly-enabled layers: (count = %u)\n", vk_ext_props_count);
	for (unsigned i = 0; i < vk_ext_props_count; ++i)
		::printf("[INFO]  - \"%s\" (spec: %u.%u.%u)\n"
			, vk_ext_props[i].extensionName
			, VK_VERSION_MAJOR(vk_ext_props[i].specVersion), VK_VERSION_MINOR(vk_ext_props[i].specVersion), VK_VERSION_PATCH(vk_ext_props[i].specVersion)
		);
	::printf("\n");

	constexpr unsigned MaxInstanceLayers = 32;
	uint32_t vk_layer_props_count = MaxInstanceLayers;
	VkLayerProperties vk_layer_props[MaxInstanceLayers];
	vkEnumerateInstanceLayerProperties(&vk_layer_props_count, vk_layer_props);

	::printf("[INFO] Layers: (count = %u)\n", vk_layer_props_count);
	for (unsigned i = 0; i < vk_layer_props_count; ++i)
	{
		vk_ext_props_count = MaxInstanceExts;
		vkEnumerateInstanceExtensionProperties(vk_layer_props[i].layerName, &vk_ext_props_count, vk_ext_props);

		::printf("[INFO]  - \"%s\" (%s) (spec: %u.%u.%u, impl: %u.%u.%u, exts: %u)\n"
			, vk_layer_props[i].layerName
			, vk_layer_props[i].description
			, VK_VERSION_MAJOR(vk_layer_props[i].specVersion), VK_VERSION_MINOR(vk_layer_props[i].specVersion), VK_VERSION_PATCH(vk_layer_props[i].specVersion)
			, vk_layer_props[i].implementationVersion, VK_VERSION_MINOR(vk_layer_props[i].implementationVersion), VK_VERSION_PATCH(vk_layer_props[i].implementationVersion)
			, vk_ext_props_count
		);

		for (unsigned i = 0; i < vk_ext_props_count; ++i)
			::printf("[INFO]    - \"%s\" (spec: %u.%u.%u)\n"
				, vk_ext_props[i].extensionName
				, VK_VERSION_MAJOR(vk_ext_props[i].specVersion), VK_VERSION_MINOR(vk_ext_props[i].specVersion), VK_VERSION_PATCH(vk_ext_props[i].specVersion)
			);
	}
	::printf("\n");

	// Check these out as well: (These are independent of an instance)
	//  vkEnumerateInstanceVersion
	//  vkEnumerateInstanceLayerProperties
	//  vkEnumerateInstanceExtensionProperties
	//  --vkCreateInstance
	//
	// Should I use these explicitly?
	//  vkGetInstanceProcAddr
	//  vkGetDeviceProcAddr
	//
	// 

	VkApplicationInfo vk_ai = { VK_STRUCTURE_TYPE_APPLICATION_INFO, nullptr };
	vk_ai.pApplicationName = "yVK00";
	vk_ai.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
	vk_ai.pEngineName = "yzt's Own";
	vk_ai.engineVersion = VK_MAKE_VERSION(0, 1, 0);
	vk_ai.apiVersion = VK_MAKE_VERSION(1, 1, 0);

	#if defined(ENABLE_VALIDATION)
	constexpr char const* vk_required_layers [] = {
		"VK_LAYER_LUNARG_standard_validation",
	};
	constexpr uint32_t vk_required_layer_count = _countof(vk_required_layers);
	#else
	constexpr char const** vk_required_layers = nullptr;
	constexpr uint32_t vk_required_layer_count = 0;
	#endif


	constexpr char const* vk_required_extensions [] = {
		VK_KHR_SURFACE_EXTENSION_NAME,
		VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
		VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
		//VK_KHR_PERFORMANCE_QUERY_EXTENSION_NAME,
	};

	constexpr VkValidationFeatureEnableEXT vk_enabled_validation_features [] {
		VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT,
		VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT,
		VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
	};
	VkValidationFeaturesEXT vk_vf_ext = { VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT, nullptr };
	vk_vf_ext.enabledValidationFeatureCount = _countof(vk_enabled_validation_features);
	vk_vf_ext.pEnabledValidationFeatures = vk_enabled_validation_features;
	vk_vf_ext.disabledValidationFeatureCount = 0;
	vk_vf_ext.pDisabledValidationFeatures = nullptr;

	VkInstanceCreateInfo vk_ici = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO, &vk_vf_ext };         // <<---
	vk_ici.flags = 0;
	vk_ici.pApplicationInfo = &vk_ai;
	vk_ici.enabledLayerCount = vk_required_layer_count;
	vk_ici.ppEnabledLayerNames = vk_required_layers;
	vk_ici.enabledExtensionCount = _countof(vk_required_extensions);
	vk_ici.ppEnabledExtensionNames = vk_required_extensions;

	VkInstance vk_instance = VK_NULL_HANDLE;
	VkAllocationCallbacks* vk_allocs_ptr = nullptr;
	vk_res = vkCreateInstance(&vk_ici, vk_allocs_ptr, &vk_instance);
	if (VK_SUCCESS != vk_res)
	{
		::printf("[ERROR] vkCreateInstance() failed.\n");
		abort();
	}

	vk_CreateDebugReportCallbackEXT =
		(PFN_vkCreateDebugReportCallbackEXT)
		vkGetInstanceProcAddr(vk_instance, "vkCreateDebugReportCallbackEXT");
	vk_DestroyDebugReportCallbackEXT =
		(PFN_vkDestroyDebugReportCallbackEXT)
		vkGetInstanceProcAddr(vk_instance, "vkDestroyDebugReportCallbackEXT");
	if (!vk_CreateDebugReportCallbackEXT || !vk_DestroyDebugReportCallbackEXT)
	{
		::printf("[ERROR] Couldn't get extension function pointers for {Create|Destroy}DebugReportCallbackEXT.\n");
		abort();
	}

	#if defined(ENABLE_VALIDATION)
	vk_EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR =
		(PFN_vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR)
		vkGetInstanceProcAddr(vk_instance, "vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR");
	if (!vk_EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR)
	{
		::printf("[ERROR] Couldn't get extension function pointer for vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR.\n");
		abort();
	}
	#endif
	VkDebugReportCallbackCreateInfoEXT vk_dcbci = { VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT, nullptr };
	vk_dcbci.flags = 0
		//| VK_DEBUG_REPORT_INFORMATION_BIT_EXT
		| VK_DEBUG_REPORT_WARNING_BIT_EXT
		| VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT
		| VK_DEBUG_REPORT_ERROR_BIT_EXT
		| VK_DEBUG_REPORT_DEBUG_BIT_EXT
		;
	vk_dcbci.pfnCallback = VulkanReportFunc;
	vk_dcbci.pUserData = nullptr;
	vk_res = vk_CreateDebugReportCallbackEXT(vk_instance, &vk_dcbci, vk_allocs_ptr, &yvkDebugCallback);
	if (VK_SUCCESS != vk_res)
	{
		::printf("[ERROR] vkCreateDebugReportCallbackEXT() failed.\n");
		abort();
	}
	/* Done. */

	/* Create SDL window and setup Vulkan use... */
	SDL_Window* window = nullptr;
	window = SDL_CreateWindow("Vulkan Test 00", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, 0 | SDL_WINDOW_VULKAN);
	//constexpr unsigned VkMaxSDLExts = 8;
	//char const * vk_ext_names [VkMaxSDLExts];
	//unsigned vk_ext_count = VkMaxSDLExts;
	//SDL_Vulkan_GetInstanceExtensions(window, &vk_ext_count, vk_ext_names);
	//::printf("[TRACE] Vulkan extensions required by SDL: (count = %u)\n", vk_ext_count);
	//for (unsigned i = 0; i < vk_ext_count; ++i)
	//    ::printf("[TRACE]  - \"%s\"\n", vk_ext_names[i]);

	//SDL_SysWMinfo sdl_wmi = {};
	//SDL_VERSION(&sdl_wmi.version);
	//SDL_GetWindowWMInfo(window, &sdl_wmi);
	//VkWin32SurfaceCreateInfoKHR vk_wsci = {VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR, nullptr};
	//vk_wsci.flags = 0;
	//vk_wsci.hinstance = ::GetModuleHandleA(nullptr);    // or sdl_wmi.info.win.instance;
	//vk_wsci.hwnd = sdl_wmi.info.win.window;
	//VkSurfaceKHR vk_surface = VK_NULL_HANDLE;
	//vk_res = vkCreateWin32SurfaceKHR(vk_instance, &vk_wsci, vk_allocs_ptr, &vk_surface);

	VkSurfaceKHR vk_surface = VK_NULL_HANDLE;
	SDL_Vulkan_CreateSurface(window, vk_instance, &vk_surface);
	/* Done. */

	/* Enumerate and select physical devices and queues to create a "device"... */
	VkPhysicalDevice vk_selected_phys_dev = nullptr;
	uint32_t vk_selected_graphics_queuefamily_index = 0xFFFF'FFFF;
	uint32_t vk_selected_present_queuefamily_index = 0xFFFF'FFFF;
	VkPhysicalDeviceFeatures vk_selected_features = {};

	constexpr unsigned MaxVulkanPhysicalDeviceCount = 8;
	uint32_t vk_phys_dev_count = MaxVulkanPhysicalDeviceCount;
	VkPhysicalDevice vk_phys_devs[MaxVulkanPhysicalDeviceCount];
	vkEnumeratePhysicalDevices(vk_instance, &vk_phys_dev_count, vk_phys_devs);
	::printf("[INFO] Found %u physical device(s).\n", vk_phys_dev_count);
	for (unsigned d = 0; d < vk_phys_dev_count; ++d)
	{
		VkPhysicalDeviceProperties props = {};
		vkGetPhysicalDeviceProperties(vk_phys_devs[d], &props);

		::printf("[INFO]  - Device #%u: \"%s\" (type: %u, id: 0x%04X, vendor: 0x%04X) (api: %u.%u.%u, driver: %u.%u.%u)\n"
			, d
			, props.deviceName
			, props.deviceType
			, props.deviceID
			, props.vendorID
			, VK_VERSION_MAJOR(props.apiVersion), VK_VERSION_MINOR(props.apiVersion), VK_VERSION_PATCH(props.apiVersion)
			, VK_VERSION_MAJOR(props.driverVersion), VK_VERSION_MINOR(props.driverVersion), VK_VERSION_PATCH(props.driverVersion)
		);

		::printf("[INFO]  - Device Limits : \n          - maxPushConstantsSize = %d \n", props.limits.maxPushConstantsSize);

		//VkPhysicalDeviceProperties2 props2 = {VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2, nullptr};
		//vkGetPhysicalDeviceProperties2(vk_phys_devs[i], &props2);

		VkPhysicalDeviceFeatures features = {};
		vkGetPhysicalDeviceFeatures(vk_phys_devs[d], &features);

		//VkPhysicalDeviceFeatures2 features2 = {VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2, nullptr};
		//vkGetPhysicalDeviceFeatures2(vk_phys_devs[i], &features2);

		if (vk_selected_phys_dev == nullptr && props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU || props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU || props.deviceType == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU)
		{
			vk_selected_phys_dev = vk_phys_devs[d];
			vk_selected_features = features;    // THIS IS BAD! Don't enabled all the features.
		}


		VkPhysicalDeviceMemoryProperties mem_props = {};
		vkGetPhysicalDeviceMemoryProperties(vk_phys_devs[d], &mem_props);
		::printf("[INFO]    Memory Heaps and Types: (%u memory heaps, and %u memory types)\n", mem_props.memoryHeapCount, mem_props.memoryTypeCount);
		for (unsigned i = 0; i < mem_props.memoryHeapCount; ++i)
		{
			::printf("[INFO]      - Heap #%u: %.1f MiB (%s%s)\n"
				, i
				, mem_props.memoryHeaps[i].size / 1'048'576.0
				, (mem_props.memoryHeaps[i].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) ? " Device-Local " : ""
				, (mem_props.memoryHeaps[i].flags & VK_MEMORY_HEAP_MULTI_INSTANCE_BIT) ? " Multi-Instance " : ""
			);
		}
		for (unsigned i = 0; i < mem_props.memoryTypeCount; ++i)
		{
			auto const& v = mem_props.memoryTypes[i];
			::printf("[INFO]      - Type #%u: on heap #%u ( %s %s %s %s %s %s %s %s )\n"
				, i
				, v.heapIndex
				, v.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT ? "Device-local" : ""
				, v.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT ? "Host-visible" : ""
				, v.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT ? "Host-coherent" : ""
				, v.propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT ? "Host-cached" : ""
				, v.propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT ? "Lazily-allocated" : ""
				, v.propertyFlags & VK_MEMORY_PROPERTY_PROTECTED_BIT ? "Protected" : ""
				, v.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD ? "Device-coherent(AMD)" : ""
				, v.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD ? "Device-uncached(AMD)" : ""
			);
		}
		constexpr unsigned MaxQueueFamilies = 32;
		VkQueueFamilyProperties qfam_props[MaxQueueFamilies] = {};
		uint32_t qfam_count = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(vk_phys_devs[d], &qfam_count, nullptr);
		if (qfam_count > MaxQueueFamilies)
		{
			::printf("[WARN] Require more queue family properties! (%u > %u)\n", qfam_count, MaxQueueFamilies);
			qfam_count = MaxQueueFamilies;
		}
		vkGetPhysicalDeviceQueueFamilyProperties(vk_phys_devs[d], &qfam_count, qfam_props);
		::printf("[INFO]    Queues: (%u queue families)\n", qfam_count);
		for (unsigned i = 0; i < qfam_count; ++i)
		{
			if (vk_phys_devs[d] == vk_selected_phys_dev && vk_selected_graphics_queuefamily_index == 0xFFFF'FFFF && qfam_props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				vk_selected_graphics_queuefamily_index = i;

				VkBool32 present_support = false;
				vkGetPhysicalDeviceSurfaceSupportKHR(vk_phys_devs[d], i, vk_surface, &present_support);

				if (qfam_props[i].queueCount > 0 && present_support)
				{
					vk_selected_present_queuefamily_index = i;
				}
			}


			constexpr unsigned MaxQueuePerfCounters = 64;
			uint32_t counter_count = MaxQueuePerfCounters;
			VkPerformanceCounterKHR counters[MaxQueuePerfCounters];
			VkPerformanceCounterDescriptionKHR descs[MaxQueuePerfCounters];
			for (unsigned j = 0; j < MaxQueuePerfCounters; ++j)
			{
				counters[j].sType = VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_KHR;
				counters[j].pNext = nullptr;
				descs[j].sType = VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_DESCRIPTION_KHR;
				descs[j].pNext = nullptr;
			}
			////vk_EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR(
			////    vk_phys_devs[d], i, &counter_count, counters, descs
			////);

			auto const& v = qfam_props[i];
			::printf("[INFO]      - Queue #%u: count=%u, TS bits=%u, min img xfer=%ux%ux%u ( %s %s %s %s %s ) (perf counters=%u)\n"
				, i
				, v.queueCount
				, v.timestampValidBits
				, v.minImageTransferGranularity.width, v.minImageTransferGranularity.height, v.minImageTransferGranularity.depth
				, v.queueFlags & VK_QUEUE_GRAPHICS_BIT ? "Graphics" : ""
				, v.queueFlags & VK_QUEUE_COMPUTE_BIT ? "Compute" : ""
				, v.queueFlags & VK_QUEUE_TRANSFER_BIT ? "Transfer" : ""
				, v.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT ? "Sparse-binding" : ""
				, v.queueFlags & VK_QUEUE_PROTECTED_BIT ? "Protected" : ""
				, counter_count
			);

		}

		::printf("\n");
	}
	::printf("\n");

	if (vk_selected_phys_dev == nullptr)
	{
		::printf("[ERROR] Couldn't find a suitable Vulkan device.\n");
		::abort();
	}
	if (vk_selected_graphics_queuefamily_index == 0xFFFF'FFFF)
	{
		::printf("[ERROR] Couldn't find a suitable Vulkan queue.\n");
		::abort();
	}

	VkPhysicalDeviceMemoryProperties vk_selected_phys_dev_mem_props = {};
	vkGetPhysicalDeviceMemoryProperties(vk_selected_phys_dev, &vk_selected_phys_dev_mem_props);

	constexpr unsigned MaxDeviceExtProps = 256;
	uint32_t dev_ext_props_count = MaxDeviceExtProps;
	VkExtensionProperties dev_ext_props[MaxDeviceExtProps];
	vkEnumerateDeviceExtensionProperties(vk_selected_phys_dev, nullptr, &dev_ext_props_count, dev_ext_props);
	::printf("[INFO] Selected device has %u extensions:\n", dev_ext_props_count);
	for (unsigned i = 0; i < dev_ext_props_count; ++i)
	{
		auto const& v = dev_ext_props[i];
		::printf("[INFO]  - \"%s\" (%u.%u.%u)\n"
			, v.extensionName
			, VK_VERSION_MAJOR(v.specVersion), VK_VERSION_MINOR(v.specVersion), VK_VERSION_PATCH(v.specVersion)
		);
	}

	VkDevice vk_device = VK_NULL_HANDLE;

	float queue_priority_one = 1.0f;
	VkDeviceQueueCreateInfo vk_dqci [] = {
		{VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, nullptr},
		{VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, nullptr},
	};
	vk_dqci[0].flags = 0;
	vk_dqci[0].queueFamilyIndex = vk_selected_graphics_queuefamily_index;
	vk_dqci[0].queueCount = 1;
	vk_dqci[0].pQueuePriorities = &queue_priority_one;
	vk_dqci[1].flags = 0;
	vk_dqci[1].queueFamilyIndex = vk_selected_present_queuefamily_index;
	vk_dqci[1].queueCount = 1;
	vk_dqci[1].pQueuePriorities = &queue_priority_one;
	uint32_t actual_queue_count = (vk_selected_graphics_queuefamily_index != vk_selected_present_queuefamily_index) ? 2 : 1;

	constexpr char const* vk_device_exts [] = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	};

	VkDeviceCreateInfo vk_dci = { VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO, nullptr };
	vk_dci.flags = 0;
	vk_dci.queueCreateInfoCount = actual_queue_count;
	vk_dci.pQueueCreateInfos = vk_dqci;
	vk_dci.enabledLayerCount = 0;
	vk_dci.ppEnabledLayerNames = nullptr;
	vk_dci.enabledExtensionCount = _countof(vk_device_exts);
	vk_dci.ppEnabledExtensionNames = vk_device_exts;
	vk_dci.pEnabledFeatures = &vk_selected_features;
	vk_res = vkCreateDevice(vk_selected_phys_dev, &vk_dci, vk_allocs_ptr, &vk_device);
	if (VK_SUCCESS != vk_res)
	{
		::printf("[ERROR] Couldn't create logical device.\n");
		::abort();
	}

	::printf("\n");
	::printf("[LOG] Device created successfully.\n");

	VkQueue vk_graphics_queue;
	VkQueue vk_present_queue;
	vkGetDeviceQueue(vk_device, vk_selected_graphics_queuefamily_index, 0, &vk_graphics_queue);
	vkGetDeviceQueue(vk_device, vk_selected_present_queuefamily_index, 0, &vk_present_queue);
	if (vk_graphics_queue == nullptr || vk_present_queue == nullptr)
	{
		::printf("[ERROR] vk_graphics_queue or vk_present_queue is nullptr.\n");
		::abort();
	}
	::printf("[LOG] Graphics and Present Queue created successfully.\n");
	::printf("\n");

	/* Get Capabilities */
	/* SwapChainExtents, MinMaxImageCount, CurrentTransform*/
	VkSurfaceCapabilitiesKHR vk_sck = {};
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vk_selected_phys_dev, vk_surface, &vk_sck);

	/* Get Supported Formats */
	constexpr uint32_t MaxFormatCount = 128;
	uint32_t format_count = MaxFormatCount;
	uint32_t vk_selected_format = 0xFFFF'FFFF;
	VkSurfaceFormatKHR vk_swapchain_formats[MaxFormatCount];
	vkGetPhysicalDeviceSurfaceFormatsKHR(vk_selected_phys_dev, vk_surface, &format_count, nullptr);
	if (format_count > MaxFormatCount)
	{
		::printf("[ERROR] Insufficient Format Count (>128). \n");
		::abort();
	}
	vkGetPhysicalDeviceSurfaceFormatsKHR(vk_selected_phys_dev, vk_surface, &format_count, vk_swapchain_formats);


	for (uint32_t i = 0; i < format_count; ++i)
	{
		::printf("[INFO] Format: %d:%s, ColorSpace: %d \n", vk_swapchain_formats[i].format, VkFormatToString(vk_swapchain_formats[i].format), vk_swapchain_formats[i].colorSpace);
		if (vk_swapchain_formats[i].format == VK_FORMAT_B8G8R8A8_UNORM && vk_swapchain_formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			vk_selected_format = i;
	}

	if (vk_selected_format == 0xFFFF'FFFF)
	{
		::printf("[ERROR] Surface not found. \n");
		::abort();
	}

	/* Get Present Modes */
	VkPresentModeKHR vk_present_modes[16];
	uint32_t present_modes_count = _countof(vk_present_modes);
	VkPresentModeKHR vk_selected_present_mode = VK_PRESENT_MODE_FIFO_KHR;
	vkGetPhysicalDeviceSurfacePresentModesKHR(vk_selected_phys_dev, vk_surface, &present_modes_count, vk_present_modes);
	for (uint32_t i = 0; i < present_modes_count; ++i)
	{
		const VkPresentModeKHR& available_present_mode = vk_present_modes[i];
		if (available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR)
		{
			vk_selected_present_mode = available_present_mode;
			break;
		}
		else if (available_present_mode == VK_PRESENT_MODE_IMMEDIATE_KHR)
		{
			vk_selected_present_mode = available_present_mode;
		}
	}


	int window_width, window_height;
	SDL_Vulkan_GetDrawableSize(window, &window_width, &window_height);
	VkExtent2D vk_swapchain_extent = VkExtent2D { (uint32_t)window_width, (uint32_t)window_height };

	uint32_t vk_calculated_image_count = std::min(vk_sck.minImageCount + 1, vk_sck.maxImageCount);

	VkSwapchainKHR vk_swapchain = VK_NULL_HANDLE;
	VkSwapchainCreateInfoKHR vk_scci = { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR, nullptr };
	vk_scci.flags = 0;
	vk_scci.surface = vk_surface;
	vk_scci.minImageCount = vk_calculated_image_count;
	vk_scci.imageFormat = vk_swapchain_formats[vk_selected_format].format;
	vk_scci.imageColorSpace = vk_swapchain_formats[vk_selected_format].colorSpace;
	vk_scci.imageExtent = vk_swapchain_extent;
	vk_scci.imageArrayLayers = 1;
	vk_scci.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	vk_scci.imageSharingMode = (vk_selected_present_queuefamily_index == vk_selected_graphics_queuefamily_index) ? VK_SHARING_MODE_EXCLUSIVE : VK_SHARING_MODE_CONCURRENT;
	vk_scci.queueFamilyIndexCount = actual_queue_count;
	uint32_t vk_queue_family_indices[2] = { vk_selected_graphics_queuefamily_index, vk_selected_present_queuefamily_index, };
	vk_scci.pQueueFamilyIndices = vk_queue_family_indices;
	vk_scci.preTransform = vk_sck.currentTransform;
	vk_scci.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	vk_scci.presentMode = vk_selected_present_mode;
	vk_scci.clipped = VK_TRUE;

	vk_res = vkCreateSwapchainKHR(vk_device, &vk_scci, vk_allocs_ptr, &vk_swapchain);

	if (vk_res != VK_SUCCESS)
	{
		::printf("[ERROR] Swapchain could not be created. \n");
		::abort();
	}

	::printf("[LOG] Swapchain Created Successfully. \n");

	VkImage vk_swapchain_images[4];
	VkImageView vk_swapchain_image_views[_countof(vk_swapchain_images)];
	uint32_t vk_swapchain_images_count = vk_calculated_image_count;
	vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &vk_swapchain_images_count, nullptr);
	if (vk_swapchain_images_count > _countof(vk_swapchain_images))
	{
		::printf("[ERROR] Insufficient Swapchain Images (>4). \n");
		::abort();
	}
	vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &vk_swapchain_images_count, vk_swapchain_images);

	for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
	{
		VkImageViewCreateInfo vk_ivci = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, nullptr };
		vk_ivci.image = vk_swapchain_images[i];
		vk_ivci.viewType = VK_IMAGE_VIEW_TYPE_2D;
		vk_ivci.format = vk_swapchain_formats[vk_selected_format].format;
		vk_ivci.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_ivci.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_ivci.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_ivci.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		vk_ivci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		vk_ivci.subresourceRange.baseArrayLayer = 0;
		vk_ivci.subresourceRange.baseMipLevel = 0;
		vk_ivci.subresourceRange.levelCount = 1;
		vk_ivci.subresourceRange.layerCount = 1;
		vk_res = vkCreateImageView(vk_device, &vk_ivci, vk_allocs_ptr, &vk_swapchain_image_views[i]);

		if (vk_res != VK_SUCCESS)
		{
			::printf("[ERROR] vkCreateImageView:%d could not be created. \n", i);
			::abort();
		}
	}

	constexpr VkFormat vk_depth_format = VK_FORMAT_D16_UNORM;
	VkImage vk_depth_images[_countof(vk_swapchain_images)];
	VkImageView vk_depth_image_views[_countof(vk_swapchain_images)];
	VkDeviceSize vk_depth_image_mem_offsets[_countof(vk_swapchain_images)];
	VkDeviceMemory vk_depth_device_mem = VK_NULL_HANDLE;
	{
		VkImageCreateInfo vk_ici_depth = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, nullptr };
		vk_ici_depth.flags = 0;
		vk_ici_depth.imageType = VK_IMAGE_TYPE_2D;
		vk_ici_depth.format = vk_depth_format;
		vk_ici_depth.extent = { vk_swapchain_extent.width, vk_swapchain_extent.height, 1 };
		vk_ici_depth.mipLevels = 1;
		vk_ici_depth.arrayLayers = 1;
		vk_ici_depth.samples = VK_SAMPLE_COUNT_1_BIT;
		vk_ici_depth.tiling = VK_IMAGE_TILING_OPTIMAL;
		vk_ici_depth.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		vk_ici_depth.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		vk_ici_depth.queueFamilyIndexCount = 1;
		vk_ici_depth.pQueueFamilyIndices = &vk_selected_graphics_queuefamily_index;
		vk_ici_depth.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
		{
			vk_res = vkCreateImage(vk_device, &vk_ici_depth, vk_allocs_ptr, &vk_depth_images[i]);
			VULKAN_CHECK_AND_FAIL(vkCreateImage);
		}

		VkMemoryRequirements vk_img_mem_req[_countof(vk_swapchain_images)];
		VkDeviceSize vk_depth_mem_size_total = 0;
		VkDeviceSize vk_depth_mem_alignment = 1;
		uint32_t vk_depth_mem_type_bits = UINT32_MAX;
		for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
		{
			vkGetImageMemoryRequirements(vk_device, vk_depth_images[i], &vk_img_mem_req[i]);

			vk_depth_mem_size_total = (vk_depth_mem_size_total + vk_img_mem_req[i].alignment - 1) / vk_img_mem_req[i].alignment * vk_img_mem_req[i].alignment;
			vk_depth_image_mem_offsets[i] = vk_depth_mem_size_total;

			vk_depth_mem_size_total += vk_img_mem_req[i].size;
			vk_depth_mem_type_bits &= vk_img_mem_req[i].memoryTypeBits;
			vk_depth_mem_alignment = std::max(vk_depth_mem_alignment, vk_img_mem_req[i].alignment);
		}

		uint32_t vk_depth_mem_selected_type = UINT32_MAX;
		for (uint32_t i = 0; i < vk_selected_phys_dev_mem_props.memoryTypeCount; ++i)
		{
			auto f = vk_selected_phys_dev_mem_props.memoryTypes[i].propertyFlags;
			if (((vk_depth_mem_type_bits & f) & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) != 0)
			{
				vk_depth_mem_selected_type = i;
				break;
			}
		}

		if (vk_depth_mem_selected_type == UINT32_MAX)
		{
			::printf("[ERROR] DepthBuffer image creation memory doesn't support VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT\n");
			::abort();
		}

		VkMemoryAllocateInfo vk_mai_depth = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr };
		vk_mai_depth.allocationSize = vk_depth_mem_size_total;
		vk_mai_depth.memoryTypeIndex = vk_depth_mem_selected_type;

		vk_res = vkAllocateMemory(vk_device, &vk_mai_depth, vk_allocs_ptr, &vk_depth_device_mem);
		VULKAN_CHECK_AND_FAIL(vkAllocateMemory);

		for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
		{
			vk_res = vkBindImageMemory(vk_device, vk_depth_images[i], vk_depth_device_mem, vk_depth_image_mem_offsets[i]);
			VULKAN_CHECK_AND_FAIL(vkBindImageMemory);
		}

		for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
		{
			VkImageViewCreateInfo vk_ivci_depth = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, nullptr };
			vk_ivci_depth.image = vk_depth_images[i];
			vk_ivci_depth.viewType = VK_IMAGE_VIEW_TYPE_2D;
			vk_ivci_depth.format = vk_depth_format;
			vk_ivci_depth.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			vk_ivci_depth.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			vk_ivci_depth.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			vk_ivci_depth.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
			vk_ivci_depth.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			vk_ivci_depth.subresourceRange.baseArrayLayer = 0;
			vk_ivci_depth.subresourceRange.baseMipLevel = 0;
			vk_ivci_depth.subresourceRange.levelCount = 1;
			vk_ivci_depth.subresourceRange.layerCount = 1;
			vk_res = vkCreateImageView(vk_device, &vk_ivci_depth, vk_allocs_ptr, &vk_depth_image_views[i]);
			VULKAN_CHECK_AND_FAIL(vkCreateImageView);
		}
	}

	/* Render Pass */
	VkRenderPass vk_renderpass_main = VK_NULL_HANDLE;

	VkAttachmentDescription vk_renderpass_main_attachments[2] = {};
	/* Color Attachment */
	vk_renderpass_main_attachments[0].flags = 0;
	vk_renderpass_main_attachments[0].format = vk_swapchain_formats[vk_selected_format].format;
	vk_renderpass_main_attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	vk_renderpass_main_attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	vk_renderpass_main_attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	vk_renderpass_main_attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	vk_renderpass_main_attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	vk_renderpass_main_attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	vk_renderpass_main_attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	/* Depth Attachment */
	vk_renderpass_main_attachments[1].flags = 0;
	vk_renderpass_main_attachments[1].format = vk_depth_format;
	vk_renderpass_main_attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	vk_renderpass_main_attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	vk_renderpass_main_attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	vk_renderpass_main_attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	vk_renderpass_main_attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	vk_renderpass_main_attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	vk_renderpass_main_attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference vk_color_attachmentref_main;
	vk_color_attachmentref_main.attachment = 0;
	vk_color_attachmentref_main.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	VkAttachmentReference vk_depth_attachmentref_main;
	vk_depth_attachmentref_main.attachment = 1;
	vk_depth_attachmentref_main.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkSubpassDescription vk_renderpass_main_subs[1] = {};
	vk_renderpass_main_subs[0].flags = 0;
	vk_renderpass_main_subs[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	vk_renderpass_main_subs[0].inputAttachmentCount = 0;
	vk_renderpass_main_subs[0].pInputAttachments = nullptr;
	vk_renderpass_main_subs[0].colorAttachmentCount = 1;
	vk_renderpass_main_subs[0].pColorAttachments = &vk_color_attachmentref_main;
	vk_renderpass_main_subs[0].pResolveAttachments = nullptr;
	vk_renderpass_main_subs[0].pDepthStencilAttachment = &vk_depth_attachmentref_main;
	vk_renderpass_main_subs[0].preserveAttachmentCount = 0;
	vk_renderpass_main_subs[0].pPreserveAttachments = nullptr;

	VkSubpassDependency vk_subpass_dependency = {};
	vk_subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	vk_subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	vk_subpass_dependency.srcAccessMask = 0;
	vk_subpass_dependency.dstSubpass = 0;
	vk_subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	vk_subpass_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	vk_subpass_dependency.dependencyFlags = 0;

	VkRenderPassCreateInfo vk_rpci = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO, nullptr };
	vk_rpci.flags = 0;
	vk_rpci.attachmentCount = _countof(vk_renderpass_main_attachments);
	vk_rpci.pAttachments = vk_renderpass_main_attachments;
	vk_rpci.subpassCount = _countof(vk_renderpass_main_subs);
	vk_rpci.pSubpasses = vk_renderpass_main_subs;
	vk_rpci.dependencyCount = 1;
	vk_rpci.pDependencies = &vk_subpass_dependency;

	vk_res = vkCreateRenderPass(vk_device, &vk_rpci, vk_allocs_ptr, &vk_renderpass_main);

	if (vk_res != VK_SUCCESS)
	{
		::printf("[ERROR] vkCreateRenderPass could not be created. \n");
		::abort();
	}

	::printf("[LOG] vkCreateRenderPass Created Successfully. \n");


	VkFramebuffer vk_framebuffers[_countof(vk_swapchain_images)];

	for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
	{
		VkImageView attachments[2] = { vk_swapchain_image_views[i], vk_depth_image_views[i] };
		VkFramebufferCreateInfo vk_fbci = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO, nullptr };
		vk_fbci.flags = 0;
		vk_fbci.renderPass = vk_renderpass_main;
		vk_fbci.attachmentCount = 2;
		vk_fbci.pAttachments = attachments;
		vk_fbci.width = (uint32_t)window_width;
		vk_fbci.height = (uint32_t)window_height;
		vk_fbci.layers = 1;
		vk_res = vkCreateFramebuffer(vk_device, &vk_fbci, vk_allocs_ptr, &vk_framebuffers[i]);
		VULKAN_CHECK_AND_FAIL(vkCreateFramebuffer);
	}


	VkCommandPool vk_command_pool = VK_NULL_HANDLE;
	VkCommandPoolCreateInfo vk_cpci = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO, nullptr };
	vk_cpci.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; //YZT
	vk_cpci.queueFamilyIndex = vk_selected_graphics_queuefamily_index;
	vk_res = vkCreateCommandPool(vk_device, &vk_cpci, vk_allocs_ptr, &vk_command_pool);
	VULKAN_CHECK_AND_FAIL(vkCreateCommandPool);

	constexpr uint32_t MaxSwapchainImages = 8;
	VkCommandBuffer vk_command_buffers[MaxSwapchainImages];
	if (_countof(vk_command_buffers) < vk_swapchain_images_count)
	{
		::printf("[ERROR] Not enough command buffers; need at least %u.\n", vk_swapchain_images_count);
		::abort();
	}

	VkCommandBufferAllocateInfo vk_cbai = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, nullptr };
	vk_cbai.commandPool = vk_command_pool;
	vk_cbai.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	vk_cbai.commandBufferCount = vk_swapchain_images_count;
	vk_res = vkAllocateCommandBuffers(vk_device, &vk_cbai, vk_command_buffers);
	VULKAN_CHECK_AND_FAIL(vkAllocateCommandBuffers);

	VkFence vk_fences_framedone[MaxSwapchainImages];
	VkSemaphore vk_semaphore_imageacquire[MaxSwapchainImages];
	VkSemaphore vk_semaphore_readytopresent[MaxSwapchainImages];

	VkFenceCreateInfo vk_fci = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr };
	vk_fci.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	VkSemaphoreCreateInfo vk_sci = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr };
	vk_sci.flags = 0;

	for (unsigned i = 0; i < vk_swapchain_images_count; ++i)
	{
		vk_res = vkCreateFence(vk_device, &vk_fci, vk_allocs_ptr, &vk_fences_framedone[i]);
		VULKAN_CHECK_AND_FAIL(vkCreateFence);

		vk_res = vkCreateSemaphore(vk_device, &vk_sci, vk_allocs_ptr, &vk_semaphore_imageacquire[i]);
		VULKAN_CHECK_AND_FAIL(vkCreateSemaphore);
		vk_res = vkCreateSemaphore(vk_device, &vk_sci, vk_allocs_ptr, &vk_semaphore_readytopresent[i]);
		VULKAN_CHECK_AND_FAIL(vkCreateSemaphore);
	}

	/* Data Buffers */

	struct Buffer
	{
		VkBuffer buffer;
		VkDeviceMemory mem;
		VkDeviceSize size;
	};
	auto CreateBuffer =
		[vk_device, vk_allocs_ptr, vk_selected_graphics_queuefamily_index, vk_selected_phys_dev_mem_props]
	(VkDeviceSize size_bytes, VkBufferUsageFlags usage, VkMemoryPropertyFlags mem_flags) -> Buffer
	{
		Buffer ret = {};
		VkResult vk_res;

		VkBufferCreateInfo bci = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO, nullptr };
		bci.flags = 0;
		bci.size = size_bytes;
		bci.usage = usage;
		bci.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		bci.queueFamilyIndexCount = 1;
		bci.pQueueFamilyIndices = &vk_selected_graphics_queuefamily_index;
		vk_res = vkCreateBuffer(vk_device, &bci, vk_allocs_ptr, &ret.buffer);
		VULKAN_CHECK_AND_FAIL(vkCreateBuffer);

		VkMemoryRequirements mem_req = {};
		vkGetBufferMemoryRequirements(vk_device, ret.buffer, &mem_req);

		ret.size = mem_req.size;

		uint32_t mem_selected_type = UINT32_MAX;
		for (uint32_t i = 0; i < vk_selected_phys_dev_mem_props.memoryTypeCount; ++i)
		{
			auto f = vk_selected_phys_dev_mem_props.memoryTypes[i].propertyFlags;
			if ((f & mem_flags) == mem_flags)
			{
				mem_selected_type = i;
				break;
			}
		}
		if (mem_selected_type == UINT32_MAX)
		{
			::printf("[ERROR] Buffer memory doesn't support the requested flags!\n");
			::abort();
		}

		VkMemoryAllocateInfo mai = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr };
		mai.allocationSize = mem_req.size;
		mai.memoryTypeIndex = mem_selected_type;
		vk_res = vkAllocateMemory(vk_device, &mai, vk_allocs_ptr, &ret.mem);
		VULKAN_CHECK_AND_FAIL(vkAllocateMemory);

		vk_res = vkBindBufferMemory(vk_device, ret.buffer, ret.mem, 0);
		VULKAN_CHECK_AND_FAIL(vkBindBufferMemory);

		return ret;
	};
	auto DestroyBuffer = [vk_device, vk_allocs_ptr]
	(Buffer& buffer) -> void
	{
		vkDestroyBuffer(vk_device, buffer.buffer, vk_allocs_ptr);
		vkFreeMemory(vk_device, buffer.mem, vk_allocs_ptr);
		buffer = {};
	};

	struct Vertex
	{
		float pos[3];
		uint8_t col[4];
	};
	static_assert(sizeof(Vertex) == 16, "");

	/* Vertex Data*/

	Vertex vert_data_tri [] = {
		{{ 1.0f, 1.0f, 0.0f}, {0, 0, 0, 255}},
		{{ -1.0f,-1.0f, 0.0f}, {0, 0, 0, 255}},
		{{ -1.0f, 1.0f, 0.0f}, {0, 0, 0, 255}},
		{{ 1.0f, -1.0f, 0.0f}, {0, 0, 0, 255}},
	};

	uint16_t indx_data_tri [] = { 0, 1, 2, 0, 3, 1 };

	auto CopyBuffer = [vk_device, vk_command_pool, vk_graphics_queue](VkBuffer& dst, VkBuffer& src, VkDeviceSize const& buffer_size) -> void
	{

		// Allocate
		VkCommandBuffer cmd = VK_NULL_HANDLE;
		VkCommandBufferAllocateInfo cmd_alloc_info = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, nullptr };
		cmd_alloc_info.commandPool = vk_command_pool;
		cmd_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		cmd_alloc_info.commandBufferCount = 1;
		auto vk_res = vkAllocateCommandBuffers(vk_device, &cmd_alloc_info, &cmd);
		VULKAN_CHECK_AND_FAIL(vkAllocateCommandBuffers);

		// Record
		VkCommandBufferBeginInfo cbbi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, nullptr };
		cbbi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		cbbi.pInheritanceInfo = nullptr;
		vkBeginCommandBuffer(cmd, &cbbi);
		{
			VkBufferCopy copy_region = {};
			copy_region.srcOffset = 0;
			copy_region.dstOffset = 0;
			copy_region.size = buffer_size;
			vkCmdCopyBuffer(cmd, src, dst, 1, &copy_region);
		}
		vkEndCommandBuffer(cmd);

		// Submit
		VkSubmitInfo si = { VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr };
		si.waitSemaphoreCount = 0;
		si.pWaitSemaphores = nullptr;
		si.pWaitDstStageMask = {};
		si.commandBufferCount = 1;
		si.pCommandBuffers = &cmd;
		si.signalSemaphoreCount = 0;
		si.pSignalSemaphores = nullptr;

		vk_res = vkQueueSubmit(vk_graphics_queue, 1, &si, VK_NULL_HANDLE);
		VULKAN_CHECK_AND_FAIL(vkQueueSubmit);

		vk_res = vkQueueWaitIdle(vk_graphics_queue);
		VULKAN_CHECK_AND_FAIL(vkQueueWaitIdle);

		vkFreeCommandBuffers(vk_device, vk_command_pool, 1, &cmd);
	};

	/* Vertex Buffer */
	Buffer vk_buffer_vert = {};
	{
		Buffer vk_buffer_vert_staging = CreateBuffer(
			sizeof(vert_data_tri),
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		);

		void* ptr = nullptr;
		vkMapMemory(vk_device, vk_buffer_vert_staging.mem, 0, vk_buffer_vert_staging.size, 0, &ptr);
		::memcpy(ptr, vert_data_tri, sizeof(vert_data_tri));
		vkUnmapMemory(vk_device, vk_buffer_vert_staging.mem);

		vk_buffer_vert = CreateBuffer(
			sizeof(vert_data_tri),
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		);

		CopyBuffer(vk_buffer_vert.buffer, vk_buffer_vert_staging.buffer, sizeof(vert_data_tri));

		DestroyBuffer(vk_buffer_vert_staging);
	}

	Buffer vk_buffer_indx = {};
	/* Index Buffer */
	{
		Buffer vk_buffer_indx_staging = CreateBuffer(
			sizeof(indx_data_tri),
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		);

		void* ptr = nullptr;
		vkMapMemory(vk_device, vk_buffer_indx_staging.mem, 0, vk_buffer_indx_staging.size, 0, &ptr);
		::memcpy(ptr, indx_data_tri, sizeof(indx_data_tri));
		vkUnmapMemory(vk_device, vk_buffer_indx_staging.mem);

		vk_buffer_indx = CreateBuffer(
			sizeof(indx_data_tri),
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		);

		CopyBuffer(vk_buffer_indx.buffer, vk_buffer_indx_staging.buffer, sizeof(indx_data_tri));

		DestroyBuffer(vk_buffer_indx_staging);
	}

	/* Shader Module */

	auto CreateShader =
		[vk_device, vk_allocs_ptr]
	(char const* source, char const* stage) -> VkShaderModule
	{
		static constexpr char sc_CompilerPath [] = "glslc.exe";
		static constexpr char sc_InputPath [] = "temp_shader_source.glsl";
		static constexpr char sc_OutputPath [] = "temp_shader_bin.spv";

		::remove(sc_InputPath);
		::remove(sc_OutputPath);

		FILE* inf = ::fopen(sc_InputPath, "wb");
		if (nullptr == inf)
		{
			abort();
		}
		::fprintf(inf, "%s", source);
		::fclose(inf);

		char compile_cmd[1000];
		::snprintf(compile_cmd, sizeof(compile_cmd) - 1, "%s -o %s -fshader-stage=%s %s"
			, sc_CompilerPath
			, sc_OutputPath
			, stage
			, sc_InputPath
		);
		::system(compile_cmd);

		FILE* outf = ::fopen(sc_OutputPath, "rb");
		if (nullptr == outf)
		{
			abort();
		}
		::fseek(outf, 0, SEEK_END);
		auto code_size = ::ftell(outf);
		::fseek(outf, 0, SEEK_SET);
		auto code_ptr = (uint32_t*)::malloc(code_size);
		if (nullptr == code_ptr) { abort(); }
		::fread(code_ptr, code_size, 1, outf);
		::fclose(outf);

		::remove(sc_InputPath);
		::remove(sc_OutputPath);

		VkShaderModule ret = VK_NULL_HANDLE;
		VkShaderModuleCreateInfo smci = { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, nullptr };
		smci.flags = 0;
		smci.codeSize = code_size;
		smci.pCode = code_ptr;
		auto vk_res = vkCreateShaderModule(vk_device, &smci, vk_allocs_ptr, &ret);
		VULKAN_CHECK_AND_FAIL(vkCreateShaderModule);

		::free(code_ptr);
		return ret;
	};

	constexpr char const* gc_ShaderSourceVertex = R"---(
        #version 450
        #extension GL_ARB_separate_shader_objects : enable

        layout(location = 0) in vec3    inPos;
        layout(location = 1) in vec4    inColor;

        layout(push_constant) uniform PushConst {
            float time;
        } pushConst;


        layout(set = 0, binding = 0) uniform UBO {
            mat4 proj;
            mat4 view;
        } ubo;

        layout(location = 0) out vec4 fragColor;

        void main()
        {
            gl_Position = ubo.proj * ubo.view * vec4(inPos * 0.5f, 1.0f);
            fragColor = vec4(1.0f, 0.0f, 0.54f, 1.0f);
        }
    )---";

	constexpr char const* gc_ShaderSourceFragment = R"---(
        #version 450
        #extension GL_ARB_separate_shader_objects : enable

        layout(location = 0) in vec4 fragColor;

        layout(location = 0) out vec4 outColor;

        void main() 
        {
            outColor = fragColor;
        }
    )---";

	VkShaderModule vk_shader_vert = CreateShader(gc_ShaderSourceVertex, "vert");
	VkShaderModule vk_shader_frag = CreateShader(gc_ShaderSourceFragment, "frag");

	/* Decriptor Set Layout */
	VkDescriptorSetLayout vk_desc_set_layout = VK_NULL_HANDLE;
	{
		VkDescriptorSetLayoutBinding dslb[1] = {};
		dslb[0].binding = 0;
		dslb[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		dslb[0].descriptorCount = 1;
		dslb[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		dslb[0].pImmutableSamplers = nullptr;

		VkDescriptorSetLayoutCreateInfo dslci = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO, nullptr };
		dslci.flags = 0;
		dslci.bindingCount = _countof(dslb);
		dslci.pBindings = dslb;
		vk_res = vkCreateDescriptorSetLayout(vk_device, &dslci, vk_allocs_ptr, &vk_desc_set_layout);
		VULKAN_CHECK_AND_FAIL(vkCreateDescriptorSetLayout);
	}

	struct UBO
	{
		alignas(16) float proj[4 * 4];
		alignas(16) float view[4 * 4];
	};

	auto set_mat_to_identity = [](float(&m)[4 * 4])
	{
		for (unsigned c = 0, i = 0; c < 4; ++c)
		{
			for (unsigned r = 0; r < 4; ++r, ++i)
			{
				if (c == r)
					m[i] = 1;
				else
					m[i] = 0;
			}
		}
	};

	/* UBO Buffer */
	Buffer ubo_buffer[_countof(vk_swapchain_images)];
	{
		for (unsigned i = 0; i < vk_swapchain_images_count; ++i)
		{
			ubo_buffer[i] = CreateBuffer(sizeof(UBO), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		}
	}

	/* Descriptor Set */
	VkDescriptorPool vk_desc_pool = VK_NULL_HANDLE;
	VkDescriptorSet vk_desc_set_ubo[_countof(vk_swapchain_images)];
	{
		VkDescriptorPoolSize pool_size = {};
		pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		pool_size.descriptorCount = vk_swapchain_images_count;

		VkDescriptorPoolCreateInfo dpci = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO, nullptr };
		dpci.flags = 0; // VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT ?
		dpci.maxSets = vk_swapchain_images_count;
		dpci.poolSizeCount = 1;
		dpci.pPoolSizes = &pool_size;
		vk_res = vkCreateDescriptorPool(vk_device, &dpci, vk_allocs_ptr, &vk_desc_pool);
		VULKAN_CHECK_AND_FAIL(vkCreateDescriptorPool);

		VkDescriptorSetLayout layouts[_countof(vk_swapchain_images)];
		for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
			layouts[i] = vk_desc_set_layout;

		VkDescriptorSetAllocateInfo dsai = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO, nullptr };
		dsai.descriptorPool = vk_desc_pool;
		dsai.descriptorSetCount = vk_swapchain_images_count;
		dsai.pSetLayouts = layouts;
		vk_res = vkAllocateDescriptorSets(vk_device, &dsai, vk_desc_set_ubo);
		VULKAN_CHECK_AND_FAIL(vkAllocateDescriptorSets);

		for (unsigned i = 0; i < vk_swapchain_images_count; ++i)
		{
			VkDescriptorBufferInfo dbi = {};
			dbi.buffer = ubo_buffer[i].buffer;
			dbi.offset = 0;
			dbi.range = sizeof(UBO);

			VkWriteDescriptorSet wds = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, nullptr };
			wds.dstSet = vk_desc_set_ubo[i];
			wds.dstBinding = 0;
			wds.dstArrayElement = 0;
			wds.descriptorCount = 1;
			wds.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			wds.pImageInfo = nullptr;
			wds.pBufferInfo = &dbi;
			wds.pTexelBufferView = nullptr;

			vkUpdateDescriptorSets(vk_device, 1, &wds, 0, nullptr);
		}
	}

	/* Graphics Pipeline */
	VkPipelineLayout vk_graphics_pipeline_layout = {};
	VkPipeline vk_graphics_pipeline;
	{
		VkPipelineShaderStageCreateInfo pssci[2] =
		{
			{ VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, nullptr },
			{ VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, nullptr },
		};

		pssci[0].flags = 0;
		pssci[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
		pssci[0].module = vk_shader_vert;
		pssci[0].pName = "main";
		pssci[0].pSpecializationInfo = nullptr;
		pssci[1].flags = 0;
		pssci[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		pssci[1].module = vk_shader_frag;
		pssci[1].pName = "main";
		pssci[1].pSpecializationInfo = nullptr;

		VkVertexInputBindingDescription vibd = {};
		vibd.binding = 0;
		vibd.stride = sizeof(Vertex);
		vibd.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		// vec4 vertex  
		VkVertexInputAttributeDescription viad[2] = {};
		viad[0].location = 0;
		viad[0].binding = 0;
		viad[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		viad[0].offset = offsetof(Vertex, pos);
		viad[1].location = 1;
		viad[1].binding = 0;
		viad[1].format = VK_FORMAT_R8G8B8A8_UNORM;
		viad[1].offset = offsetof(Vertex, col);

		VkPipelineVertexInputStateCreateInfo vis = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO, nullptr };
		vis.flags = 0;
		vis.vertexBindingDescriptionCount = 1;
		vis.pVertexBindingDescriptions = &vibd;
		vis.vertexAttributeDescriptionCount = _countof(viad);
		vis.pVertexAttributeDescriptions = viad;

		VkPipelineInputAssemblyStateCreateInfo ias = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr };
		ias.flags = 0;
		ias.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		ias.primitiveRestartEnable = VK_FALSE;

		//VkPipelineTessellationStateCreateInfo ts = { VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO, nullptr };

		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)window_width;
		viewport.height = (float)window_height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		VkRect2D scissor = { { 0, 0 }, { (uint32_t)window_width, (uint32_t)window_height } };

		VkPipelineViewportStateCreateInfo vs = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO, nullptr };
		vs.flags = 0;
		vs.viewportCount = 1;
		vs.pViewports = &viewport;
		vs.scissorCount = 1;
		vs.pScissors = &scissor;

		VkPipelineRasterizationStateCreateInfo rs = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO, nullptr };
		rs.flags = 0;
		rs.depthClampEnable = VK_FALSE;
		rs.rasterizerDiscardEnable = VK_FALSE;
		rs.polygonMode = VK_POLYGON_MODE_FILL;
		rs.cullMode = VK_CULL_MODE_NONE; // YZT
		rs.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rs.depthBiasEnable = VK_FALSE;
		rs.depthBiasConstantFactor = 0;
		rs.depthBiasClamp = 0;
		rs.depthBiasSlopeFactor = 0;
		rs.lineWidth = 1.0f;

		VkPipelineMultisampleStateCreateInfo ms = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO, nullptr };
		ms.flags = 0;
		ms.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		ms.sampleShadingEnable = VK_FALSE;
		ms.minSampleShading = 1.0f;
		ms.pSampleMask = nullptr;
		ms.alphaToCoverageEnable = VK_FALSE;
		ms.alphaToOneEnable = VK_FALSE;

		VkPipelineDepthStencilStateCreateInfo dss = { VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO, nullptr };
		dss.flags = 0;
		dss.depthTestEnable = VK_TRUE;
		dss.depthWriteEnable = VK_TRUE;
		dss.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		dss.depthBoundsTestEnable = VK_TRUE;
		dss.stencilTestEnable = VK_FALSE;
		dss.front = {};
		dss.back = {};
		dss.minDepthBounds = 0.0f;
		dss.maxDepthBounds = 1.0f;

		VkPipelineColorBlendAttachmentState cbas = {};
		cbas.blendEnable = VK_FALSE;
		cbas.srcColorBlendFactor = {};
		cbas.dstColorBlendFactor = {};
		cbas.colorBlendOp = {};
		cbas.srcAlphaBlendFactor = {};
		cbas.dstAlphaBlendFactor = {};
		cbas.alphaBlendOp = {};
		cbas.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

		VkPipelineColorBlendStateCreateInfo cbs = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO, nullptr };
		cbs.flags = 0;
		cbs.logicOpEnable = VK_FALSE;
		cbs.logicOp = {};
		cbs.attachmentCount = 1;
		cbs.pAttachments = &cbas;
		//cbs.blendConstants;

		VkPipelineDynamicStateCreateInfo ds = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO, nullptr };
		ds.flags = 0;
		ds.dynamicStateCount = 0;
		ds.pDynamicStates = nullptr;

		VkPushConstantRange push_constant_range = {};
		push_constant_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		push_constant_range.offset = 0;
		push_constant_range.size = sizeof(float);

		VkPipelineLayoutCreateInfo plci = { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO, nullptr };
		plci.flags = 0;
		plci.setLayoutCount = 1;
		plci.pSetLayouts = &vk_desc_set_layout;
		plci.pushConstantRangeCount = 1;
		plci.pPushConstantRanges = &push_constant_range;
		vk_res = vkCreatePipelineLayout(vk_device, &plci, vk_allocs_ptr, &vk_graphics_pipeline_layout);
		VULKAN_CHECK_AND_FAIL(vkCreatePipelineLayout);

		VkGraphicsPipelineCreateInfo gpci = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO, nullptr };
		gpci.flags = 0;
		gpci.stageCount = _countof(pssci);
		gpci.pStages = pssci;
		gpci.pVertexInputState = &vis;
		gpci.pInputAssemblyState = &ias;
		gpci.pTessellationState = nullptr;
		gpci.pViewportState = &vs;
		gpci.pRasterizationState = &rs;
		gpci.pMultisampleState = &ms;
		gpci.pDepthStencilState = &dss;
		gpci.pColorBlendState = &cbs;
		gpci.pDynamicState = &ds;
		gpci.layout = vk_graphics_pipeline_layout;
		gpci.renderPass = vk_renderpass_main;
		gpci.subpass = 0;
		gpci.basePipelineHandle = VK_NULL_HANDLE;
		gpci.basePipelineIndex = 0;
		vk_res = vkCreateGraphicsPipelines(vk_device, VK_NULL_HANDLE, 1, &gpci, vk_allocs_ptr, &vk_graphics_pipeline);
		VULKAN_CHECK_AND_FAIL(vkCreateGraphicsPipelines);
	}

	constexpr uint32_t instance_count = 1;

	float time = 0;

	/* Record the commands */
	for (unsigned i = 0; i < vk_swapchain_images_count; ++i);
	/* Main Loop */
	double inverse_clock_freq = 1.0 / SDL_GetPerformanceFrequency();
	uint32_t current_frame_index = 0;
	uint32_t frame_count = 1;
	bool should_quit = false;
	double t0 = inverse_clock_freq * SDL_GetPerformanceCounter();
	while (!should_quit)
	{
		SDL_Event ev;
		while (SDL_PollEvent(&ev))
		{
			if (SDL_QUIT == ev.type)
				should_quit = true;
		}

		vk_res = vkWaitForFences(vk_device, 1, &vk_fences_framedone[current_frame_index], VK_FALSE, ~(0ULL));
		VULKAN_CHECK_AND_FAIL(vkWaitForFences);

		uint32_t target_image_index = ~0U;
		vk_res = vkAcquireNextImageKHR(vk_device, vk_swapchain, ~(0ULL), vk_semaphore_imageacquire[current_frame_index], VK_NULL_HANDLE, &target_image_index);
		if (VK_SUBOPTIMAL_KHR == vk_res)
		{
			::printf("[WARN] Acquired image is suboptimal! Probably should recreate swapchain.\n");
		}
		else if (VK_ERROR_OUT_OF_DATE_KHR == vk_res)
		{
			::printf("[ERROR] Acquired image is out-of-date! Definitely should recreate swapchain.\n");
		}
		else if (VK_SUCCESS != vk_res && VK_NOT_READY != vk_res)
		{
			::printf("[ERROR] Some other thing went wrong in image acquisition! Definitely should recreate swapchain and probably more.\n");
		}

		// Update Buffers
		{
			UBO* ubo = nullptr;
			vkMapMemory(vk_device, ubo_buffer[target_image_index].mem, 0, sizeof(UBO), 0, reinterpret_cast<void**>(&ubo));
			set_mat_to_identity(ubo->proj);
			set_mat_to_identity(ubo->view);
			ubo->view[12] += 0.25f;
			vkUnmapMemory(vk_device, ubo_buffer[target_image_index].mem);
		}

		// Record Commands
		{
			{
				VkCommandBufferBeginInfo vk_cbbi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, nullptr };
				vk_cbbi.flags = 0;
				vk_cbbi.pInheritanceInfo = nullptr;
				vkBeginCommandBuffer(vk_command_buffers[target_image_index], &vk_cbbi);
				{
					VkClearValue vk_clear_values[2] = {};
					vk_clear_values[0].color = { 0.8f, 0.8f, 0.8f, 1.0f };
					vk_clear_values[1].depthStencil = { 1.0f, 0 };

					VkRenderPassBeginInfo vk_rpbi = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO, nullptr };
					vk_rpbi.renderPass = vk_renderpass_main;
					vk_rpbi.framebuffer = vk_framebuffers[target_image_index];
					vk_rpbi.renderArea = { {0, 0}, vk_swapchain_extent };
					vk_rpbi.clearValueCount = _countof(vk_clear_values);
					vk_rpbi.pClearValues = vk_clear_values;
					vkCmdBeginRenderPass(vk_command_buffers[target_image_index], &vk_rpbi, VK_SUBPASS_CONTENTS_INLINE);
					{
						vkCmdBindPipeline(vk_command_buffers[target_image_index], VK_PIPELINE_BIND_POINT_GRAPHICS, vk_graphics_pipeline);
						VkDeviceSize offset = 0;
						vkCmdPushConstants(vk_command_buffers[target_image_index], vk_graphics_pipeline_layout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(float), &time);
						vkCmdBindDescriptorSets(
							vk_command_buffers[target_image_index],
							VK_PIPELINE_BIND_POINT_GRAPHICS,
							vk_graphics_pipeline_layout,
							0, 1,
							&vk_desc_set_ubo[target_image_index],
							0, nullptr
						);
						//@TEST
						vkCmdBindVertexBuffers(vk_command_buffers[target_image_index], 0, 1, &vk_buffer_vert.buffer, &offset);
						vkCmdBindIndexBuffer(vk_command_buffers[target_image_index], vk_buffer_indx.buffer, 0, VK_INDEX_TYPE_UINT16);
						vkCmdDrawIndexed(vk_command_buffers[target_image_index], 6, instance_count, 0, 0, 0);
					}
					vkCmdEndRenderPass(vk_command_buffers[target_image_index]);
				}
				vkEndCommandBuffer(vk_command_buffers[target_image_index]);
			}

			VkPipelineStageFlags wait_flags = 0 | VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			VkSubmitInfo si = { VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr };
			si.waitSemaphoreCount = 1;
			si.pWaitSemaphores = &vk_semaphore_imageacquire[target_image_index];
			si.pWaitDstStageMask = &wait_flags;
			si.commandBufferCount = 1;
			si.pCommandBuffers = &vk_command_buffers[target_image_index];
			si.signalSemaphoreCount = 1;
			si.pSignalSemaphores = &vk_semaphore_readytopresent[target_image_index];

			vkResetFences(vk_device, 1, &vk_fences_framedone[current_frame_index]);
			vkQueueSubmit(vk_graphics_queue, 1, &si, vk_fences_framedone[current_frame_index]);
		}

		{
			VkPresentInfoKHR pik = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, nullptr };
			pik.waitSemaphoreCount = 1;
			pik.pWaitSemaphores = &vk_semaphore_readytopresent[target_image_index];
			pik.swapchainCount = 1;
			pik.pSwapchains = &vk_swapchain;
			pik.pImageIndices = &target_image_index;
			pik.pResults = nullptr;
			vk_res = vkQueuePresentKHR(vk_present_queue, &pik);
			if (VK_SUBOPTIMAL_KHR == vk_res)
			{
				::printf("[WARN] vkQueuePresentKHR is suboptimal! Probably should recreate swapchain.\n");
			}
			else if (VK_ERROR_OUT_OF_DATE_KHR == vk_res)
			{
				::printf("[ERROR] vkQueuePresentKHR is out-of-date! Definitely should recreate swapchain.\n");
			}
			else if (VK_SUCCESS != vk_res && VK_NOT_READY != vk_res)
			{
				::printf("[ERROR] Some other thing went wrong in vkQueuePresentKHR! Definitely should recreate swapchain and probably more.\n");
			}
		}

		double t = inverse_clock_freq * SDL_GetPerformanceCounter();
		time = (float)t;
		if (t - t0 >= 1.0)
		{
			::printf("Average Frame Time: %.5f ms \n", ((t - t0) * 1000.0 / frame_count));
			t0 = t;
			frame_count = 0;
		}

		frame_count += 1;

		current_frame_index = (current_frame_index + 1);
		if (current_frame_index >= vk_swapchain_images_count)
			current_frame_index = 0;
	}

	/**/

	/* Done. */

	/* Clean everything up... */
	::printf("[LOG] Starting the clean up...\n");

	vkDeviceWaitIdle(vk_device);

	vkDestroyPipeline(vk_device, vk_graphics_pipeline, vk_allocs_ptr);
	vkDestroyPipelineLayout(vk_device, vk_graphics_pipeline_layout, vk_allocs_ptr);
	vkDestroyShaderModule(vk_device, vk_shader_vert, vk_allocs_ptr);
	vkDestroyShaderModule(vk_device, vk_shader_frag, vk_allocs_ptr);

	DestroyBuffer(vk_buffer_vert);
	DestroyBuffer(vk_buffer_indx);

	for (unsigned i = 0; i < vk_swapchain_images_count; ++i)
	{
		DestroyBuffer(ubo_buffer[i]);
	}

	vkDestroyDescriptorSetLayout(vk_device, vk_desc_set_layout, vk_allocs_ptr);
	// vkFreeDescriptorSets(vk_device, vk_desc_pool, vk_swapchain_images_count, vk_desc_set_ubo);
	vkDestroyDescriptorPool(vk_device, vk_desc_pool, vk_allocs_ptr);

	for (unsigned i = 0; i < vk_swapchain_images_count; ++i)
	{
		vkDestroyFramebuffer(vk_device, vk_framebuffers[i], vk_allocs_ptr);
	}

	for (unsigned i = 0; i < vk_swapchain_images_count; ++i)
	{
		vkDestroyImageView(vk_device, vk_depth_image_views[i], vk_allocs_ptr);
		vkDestroyImage(vk_device, vk_depth_images[i], vk_allocs_ptr);
	}

	vkFreeMemory(vk_device, vk_depth_device_mem, vk_allocs_ptr);

	for (unsigned i = 0; i < vk_swapchain_images_count; ++i)
	{
		vkDestroyFence(vk_device, vk_fences_framedone[i], vk_allocs_ptr);
		vkDestroySemaphore(vk_device, vk_semaphore_imageacquire[i], vk_allocs_ptr);
		vkDestroySemaphore(vk_device, vk_semaphore_readytopresent[i], vk_allocs_ptr);
	}
	//NOT NEEDED//vkFreeCommandBuffers(vk_device, vk_command_pool, vk_swapchain_images_count, vk_command_buffers);
	vkDestroyCommandPool(vk_device, vk_command_pool, vk_allocs_ptr);
	for (uint32_t i = 0; i < vk_swapchain_images_count; ++i)
		vkDestroyImageView(vk_device, vk_swapchain_image_views[i], vk_allocs_ptr);
	vkDestroyRenderPass(vk_device, vk_renderpass_main, vk_allocs_ptr);
	vkDestroySwapchainKHR(vk_device, vk_swapchain, vk_allocs_ptr);

	vkDestroyDevice(vk_device, vk_allocs_ptr);
	vkDestroySurfaceKHR(vk_instance, vk_surface, vk_allocs_ptr);
	SDL_DestroyWindow(window);

	vk_DestroyDebugReportCallbackEXT(vk_instance, yvkDebugCallback, vk_allocs_ptr);
	vkDestroyInstance(vk_instance, vk_allocs_ptr);
	SDL_Quit();
	::printf("[LOG] Done.\n");
	return 0;
}

#elif 1

#endif